import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:io' show Platform;

class Scanner extends StatefulWidget {
  @override
  _ScannerState createState() => _ScannerState();
}

class _ScannerState extends State<Scanner> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  late Barcode result;
  late QRViewController controller;
  String scanDisplay = "Scanning the QR code";
  String schoolShortName = "School: Scanning...";
  String studentName = "Student: Scanning...";
  String documentType = "Diploma: Scanning...";
  String mention = "Mention: Scanning...";


  @override
  void reassemble()  {
    super.reassemble();
    if  (Platform.isAndroid ) {
      controller.pauseCamera();
    }   if (Platform.isIOS) {
         controller.resumeCamera();
        }
    }


  @override
  void dispose() {
    controller?. dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scanner'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Text('${this.scanDisplay}'),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('${this.schoolShortName}'),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('${this.studentName}'),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('${this.documentType}'),
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('${this.mention}'),
          ),
          Center(
            child: Container(
              padding: EdgeInsets.all(20),
              width: 300,
              height: 300,
              child: QRView(
                key: qrKey,
                onQRViewCreated: _onQRViewCreated,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller){
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      result = scanData;
      String scanResult = result.code;
      var data = scanResult.split("_");
      if(data.length != 4){
        setState(() {
          this.scanDisplay = "Error!!!! fake diploma or qr code";
        });
      }
      else{
        setState(() {
          this.scanDisplay = "Scanner result";
          this.schoolShortName = "School: ${data[0]}";
          this.studentName = "Student Name: ${data[1]}";
          this.documentType = "Diploma: ${data[2]}";
          this.mention = "Mention: ${data[3]}";

          controller.pauseCamera();
        });
      }
    });
  }

}
